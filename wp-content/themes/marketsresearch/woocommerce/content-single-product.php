<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class('row'); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		//do_action( 'woocommerce_before_single_product_summary' );
	?>
	<div class="col-sm-8">
		<div class="theme-custom-wrapper-left">
			
			<div class="summary entry-summary">

				<?php
					/**
					 * woocommerce_single_product_summary hook.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					do_action( 'woocommerce_single_product_summary' );
				?>
			<div class="report_meta clearfix">
				<p class="report_publisher">Published: <?php echo get_field('published_by'); ?></p>
				<p class="report_date">On: <?php echo get_the_date(); ?></p>
				<p class="report_pages">Pages: <?php echo get_field('pages', $recent["ID"]); ?></p>
				<p class="report_id">Report ID: <?php echo get_the_ID(); ?></p>
			</div>
			</div><!-- .summary -->
			<?php wc_get_template( 'single-product/tabs/tabs.php' ); ?>

		</div>
			<?php
				/**
				 * woocommerce_after_single_product_summary hook.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
			?>		
	</div>
	<div class="theme-custom-wrapper-right col-md-4">
		<?php 
			do_action('woocommerce_variable_add_to_cart');
		?>
		<div class="rating-wrapper">
			<?php $rating = get_field('rating'); ?>
			<?php 
			if ($rating) {
				echo "<h2 class='widgettitle'>Over all rating</h2>";
				echo "<div class='star-inner'>";
				if ($rating == 'one') { ?>
				<span class="star"></span>
				<?php } elseif ($rating == 'two') { ?>
				<span class="star"></span>
				<span class="star"></span>
				<?php } elseif ($rating == 'three') { ?>
				<span class="star"></span>
				<span class="star"></span>
				<span class="star"></span>
				<?php } elseif ($rating == 'four') { ?>
				<span class="star"></span>
				<span class="star"></span>
				<span class="star"></span>
				<span class="star"></span>
				<?php } elseif ($rating == 'five') { ?>
				<span class="star"></span>
				<span class="star"></span>
				<span class="star"></span>
				<span class="star"></span>
				<span class="star"></span>
				<?php } ?>
				</div>
			<?php 
			}
			?>			
		</div>
		<?php if ( is_active_sidebar( 'product-sidebar' ) ) : ?>
			<div id="woocommerce-sidebar">
				<?php dynamic_sidebar( 'product-sidebar' ); ?>
			</div>
		<?php endif; ?>

	</div>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
