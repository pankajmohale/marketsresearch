<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marketsresearch
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body <?php body_class(); ?>>
	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<div class="row" id="header-top">
				<div class="site-branding col-md-5 col-sm-12">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo of_get_option('logo'); ?>" alt="logo" ></a>
				</div><!-- .site-branding -->
				<div class="col-md-7 col-sm-12 clearfix" id="header-social">
					<div id="header-social-site">
						<ul>
							<li class="facebook-social"><a href="http://<?php echo of_get_option('facebook_url'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li class="twitter-social"><a href="http://<?php echo of_get_option('twitter_url'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li class="linkedin-social"><a href="http://<?php echo of_get_option('linkedin_url'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li class="skype-social"><a href="http://<?php echo of_get_option('skype_url'); ?>" target="_blank"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div id="header-phone">
						<a href="tel:<?php  echo of_get_option('site_phone'); ?>"><?php echo of_get_option('site_phone'); ?></a>
					</div>
					<div id="header-mail">
						<a href="mailto:<?php echo of_get_option('site_email'); ?>"><?php echo of_get_option('site_email'); ?></a>
					</div>
				</div>
			</div>	
		</div>
		<div class="container-full" id="top_menu">
			<div class="container">
				<div class="row" id="header-menu">
					<div class="col-md-12">
						<nav class="navbar navbar-inverse " role="navigation">
					        <!-- Brand and toggle get grouped for better mobile display -->
					        <div class="navbar-header">
					            <button type="button"  class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					                <span class="sr-only">Toggle navigation</span>
				    	            <span class="icon-bar"></span>
				        	        <span class="icon-bar"></span>
				            	    <span class="icon-bar"></span>
				            	</button>
				        	</div><!--end navbar-header-->
        					<div class="collapse navbar-collapse menu-primary" id="bs-example-navbar-collapse-1">
				            <?php
				            	wp_nav_menu( array(
				                	'menu'              => 'Menu1',
				                	'theme_location'    => 'primary',
				                	'depth'             => 2,
				                	'container'         => '',
				                	'container_class'   => 'collapse navbar-collapse',
				                	'container_id'      => 'bs-example-navbar-collapse-1',
				                	'menu_class'        => 'nav navbar-nav',
				                	'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				                	'walker'            => new wp_bootstrap_navwalker())
				            	);
				            ?>
   					 		</div><!--end navbar-colapse-->
	 					</nav>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<?php if(!is_front_page()){ ?>
	<div class="container-full" id="title_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 page_title">
					<?php if(is_home()){
						echo "<h1>".get_the_title( get_option('page_for_posts', true) )."</h1>";
					}elseif(is_single()){
						$categories = get_the_category();
						if ( ! empty( $categories ) ) {
						    echo "<h2>".esc_html( $categories[0]->name )."</h2>";   
						}
					}elseif(is_archive()){
				        woocommerce_page_title();     
				    }else{
						echo "<h1>".get_the_title()."</h1>";
					} ?>
				</div>
				<div class="col-sm-6 col-md-8 page_bread">
					<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
					    <?php if(function_exists('bcn_display'))
					    {
					        bcn_display();
					    }?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-full" id="main-content">
		<div class="banner-wrapper">
			<?php $banner_contact = get_field( "banner_contact" ); ?>
			<?php if ($banner_contact == 'banner') { ?>
			<?php $banner = get_field( "banner" );
			if ($banner != '') { ?>
				<img src="<?php echo get_field( "banner" ); ?>">
			<?php } ?>
			<?php } else { ?>
				<?php $map_val = get_field( "map_link" ); 
				if ($map_val != '') { ?>
					<iframe src="<?php echo $map_val; ?>" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
 				<?php 
					}
				?>
			<?php } ?>
		</div>
		<div class="container">
			<div id="content" class="site-content">
				
			</div>
	<?php } ?>