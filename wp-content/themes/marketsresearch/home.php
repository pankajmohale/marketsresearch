<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package marketsresearch
 */

get_header(); ?>
	<div class="row">
    	<div class="col-md-12 col-xs-12">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
				<?php
				if ( have_posts() ) : ?>
					<div class="row">
						<?php /* Start the Loop */
						while ( have_posts() ) : the_post(); ?>

						<div class="col-sm-6 col-md-4">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
								<a href="<?php echo get_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail(array(360, 200)); ?></a>
								<div class="post-content">
									<header class="entry-header">
										<?php
										if ( is_single() ) :
											the_title( '<h1 class="entry-title">', '</h1>' );
										else :
											the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
										endif; ?>
										<div class="report_meta clearfix">
											<p class="report_publisher">Published: <?php echo the_author_meta( 'display_name', $recent["post_author"] ); ?></p>
											<p class="report_date">On: <?php echo get_the_date(); ?></p>
										</div>
									</header><!-- .entry-header -->

									<div class="entry-content">
										<p class="report_exc"><?php echo substr(strip_tags(get_the_content()), 0, 150); ?> [...] <a href="<?php echo get_permalink(); ?>">Read More</a></p>
										<?php
											wp_link_pages( array(
												'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'marketsresearch' ),
												'after'  => '</div>',
											) );
										?>
									</div><!-- .entry-content -->
								</div>	
							</article><!-- #post-## -->
						</div>
					<?php endwhile; ?>
					</div>	
					<?php wp_pagenavi();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>

					</main><!-- #main -->
				</div><!-- #primary -->
		    </div><!--col-md-8 col-xs-12 -->
		<?php
			get_footer();