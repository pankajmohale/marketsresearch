<?php
 /*
 Template Name: Home Full Width
 */
 ?>

<?php 
get_header(); ?>
<div class="container-full" id="home_search">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
					<input type="search" class="search-field" placeholder="enter your text" name="s" />
					<?php
						$swp_cat_dropdown_args = array(
								'show_option_all'  => __( 'select categories' ),
								'name'             => 'market_category_limiter',
								'taxonomy'         => 'product_cat',
								'hide_empty'       => 0,
							);
						wp_dropdown_categories( $swp_cat_dropdown_args );
					?>
					<input type="hidden" name="post_type" value="product" /> 
					<input type="submit" class="search-submit" value="Search" />
				</form>
			</div>
		</div>
	</div>	
</div>
<div class="container" id="feature-box">
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="box">
				<p class="image-box"><img src="<?php echo of_get_option('feature-img-1');?>" alt="feature-img" /></p>
				<h3><?php echo of_get_option('feature-heading-1'); ?></h3>
				<p><?php echo of_get_option('feature-text-1'); ?></p>
			</div>	
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="box">
				<p class="image-box"><img src="<?php echo of_get_option('feature-img-2');?>" alt="feature-img" /></p>
				<h3><?php echo of_get_option('feature-heading-2'); ?></h3>
				<p><?php echo of_get_option('feature-text-2'); ?></p>
			</div>	
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="box">
				<p class="image-box"><img src="<?php echo of_get_option('feature-img-3');?>" alt="feature-img" /></p>
				<h3><?php echo of_get_option('feature-heading-3'); ?></h3>
				<p><?php echo of_get_option('feature-text-3'); ?></p>
			</div>
		</div>
	</div>
</div>
<div class="container-full" id="home-about">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="colored-border"><?php echo of_get_option('home-about-heading'); ?></h1>
				<p><?php echo of_get_option('home-about-text'); ?></p>
				<p><a class="read-more" href="<?php echo get_page_link(of_get_option('home-about-link')); ?>">Read More</a></p>
			</div>
		</div>
	</div>
</div>
<div class="container-full" id="home-report-cat">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="colored-border">Report Categories</h2>
			</div>
		</div>
		<div class="row">	
			<?php 
				$report_cats = of_get_option('report_categories');  
				foreach ($report_cats as $key => $value) {
					if($value == 1){
						$report_cat = get_term_by('id', $key, 'product_cat'); 
						$thumbnail_id = get_woocommerce_term_meta( $key, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
						?>
						<div class="col-sm-4 col-md-4">
							<div class="cat_box" style="background-image:url('<?php echo $image; ?>');">
								<h3><?php echo $report_cat->name; ?></h3>
								<p><?php echo substr($report_cat->description, 0, 100); ?>...</p>
								<p class="cat-read-more"><a href="<?php echo get_term_link($key, 'product_cat'); ?>">Know More</a></p>
							</div>
						</div>
					<?php }
				}
			?>
			
		</div>
		<div class="row">
			<div class="col-md-12">
				<a class="read-more" href="<?php echo get_page_link(of_get_option('home-report-cat-link')); ?>">Browse all categories</a>
			</div>
		</div>
	</div>
</div>
<div class="container-full" id="report_post_tabs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div id="myTabs">
				  	<ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#latest-report" aria-controls="latest-report" role="tab" data-toggle="tab">Latest Report</a></li>
					    <li role="presentation"><a href="#latest-news" aria-controls="latest-news" role="tab" data-toggle="tab">Latest News</a></li>
				    </ul>
				  	<div class="tab-content">
					  	<div role="tabpanel" class="tab-pane fade in active" id="latest-report">
					  		<?php
					  			$args = array( 
					  				'numberposts' => '5', 
					  				'post_type' => 'product',
									'post_status' => 'publish'
									);
								$recent_posts = wp_get_recent_posts($args);
								foreach( $recent_posts as $recent ){ ?>
									<div class="tab-box">
										<h3><?php echo $recent["post_title"]; ?></h3>
										<div class="report_meta clearfix">
											<p class="report_publisher">Published: <?php echo get_field('published_by', $recent["ID"]); ?></p>
											<p class="report_date">On: <?php echo get_the_date(); ?></p>
											<p class="report_pages">Pages: <?php echo get_field('pages', $recent["ID"]); ?></p>
											<p class="report_id">Report ID: <?php echo $recent["ID"]; ?></p>
										</div>
										<p class="report_exc"><?php echo substr(strip_tags($recent["post_content"]), 0, 450); ?> [...] <a href="<?php echo get_permalink($recent["ID"]); ?>">Read More</a></p>
									</div>
								<?php }
								wp_reset_query(); 
							?><p><a class="read-more" href="<?php echo get_page_link(of_get_option('home-report-link')); ?>">Browse all reports</a></p>
					  	</div>
					  	<div role="tabpanel" class="tab-pane fade in" id="latest-news">
					  		<?php
					  			$args = array('numberposts' => '5');
								$recent_posts1 = wp_get_recent_posts($args);
								foreach( $recent_posts1 as $recent ){ ?>
									<div class="tab-box">
										<h3><?php echo $recent["post_title"]; ?></h3>
										<div class="report_meta clearfix">
											<p class="report_publisher">Published: <?php echo the_author_meta( 'display_name', $recent["post_author"] ); ?></p>
											<p class="report_date">On: <?php echo get_the_date(); ?></p>
										</div>
										<p class="report_exc"><?php echo substr(strip_tags($recent["post_content"]), 0, 450); ?> [...] <a href="<?php echo get_permalink($recent["ID"]); ?>">Read More</a></p>
									</div>
								<?php }
								wp_reset_query();
							?>
							<p><a class="read-more" href="<?php echo get_page_link(of_get_option('home-news-link')); ?>">Browse all news</a></p>
					  	</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();