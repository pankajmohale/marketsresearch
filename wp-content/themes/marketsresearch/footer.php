<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marketsresearch
 */

?>
<?php if(!is_front_page()){ ?>
</div>
</div><!-- #content -->
</div><!-- .container -->
<?php } ?>
<div class="container-full" id="footer-top">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3">
				<?php dynamic_sidebar( 'footer-1' ); ?>
			</div>
			<div class="col-sm-6 col-md-3">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
			<div class="col-sm-6 col-md-3">
				<?php dynamic_sidebar( 'footer-3' ); ?>
			</div>
			<div class="col-sm-6 col-md-3">
				<?php dynamic_sidebar( 'footer-4' ); ?>
			</div>
		</div>
	</div>
</div>
<div class="container-full" id="footer-bottom">
	<div class="container">
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="row">
				<div class="col-sm-6 col-md-6" id="copyright">
					<p><?php echo of_get_option('footer_copyright'); ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo of_get_option('footer_logo'); ?>" /></a></p>
				</div>
				<div class="col-sm-6 col-md-6" id="footer_social">
					<ul>
							<li class="facebook-social"><a href="http://<?php echo of_get_option('facebook_url'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li class="twitter-social"><a href="http://<?php echo of_get_option('twitter_url'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li class="linkedin-social"><a href="http://<?php echo of_get_option('linkedin_url'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li class="skype-social"><a href="http://<?php echo of_get_option('skype_url'); ?>" target="_blank"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
						</ul>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- #container -->
</div>

<?php wp_footer(); ?>
<script type="text/javascript">
	jQuery('#myTabs .nav-tabs a').click(function (e) {
	  e.preventDefault()
	  jQuery(this).tab('show')
	})
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93758027-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
