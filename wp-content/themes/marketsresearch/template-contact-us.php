<?php
 /*
 Template Name: Contact Us
 */
 ?>

<?php 
get_header(); ?>
<div class="row">
<div class="col-md-12 col-xs-12">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="content-area-details">
			<div class="row">
				<div class="col-sm-4">
					<div class="contact-details">
						<span class="contact-address icon"><i class="fa fa-home" aria-hidden="true"></i></span>
						<span class="contact-address"><?php echo of_get_option('footer_address'); ?></span>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact-details">
						<span class="contact-address icon phone"><i class="fa fa-phone" aria-hidden="true"></i></span>
						<span class="contact-address"><?php echo of_get_option('site_phone'); ?></span>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact-details">
						<span class="contact-address icon mail"><i class="fa fa-envelope-open-o" aria-hidden="true"></i></span>
						<span class="contact-address"><?php echo of_get_option('site_email'); ?></span>
					</div>
				</div>
			</div>
		</div>
			<?php
			while ( have_posts() ) : the_post();

				the_content();

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();