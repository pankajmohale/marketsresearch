<?php
/**
 * marketsresearch functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package marketsresearch
 */

if ( ! function_exists( 'marketsresearch_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function marketsresearch_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on marketsresearch, use a find and replace
	 * to change 'marketsresearch' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'marketsresearch', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'marketsresearch' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'marketsresearch_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'marketsresearch_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function marketsresearch_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'marketsresearch_content_width', 640 );
}
add_action( 'after_setup_theme', 'marketsresearch_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function marketsresearch_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'marketsresearch' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'marketsresearch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 1', 'marketsresearch' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'marketsresearch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 2', 'marketsresearch' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'marketsresearch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 3', 'marketsresearch' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'marketsresearch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 4', 'marketsresearch' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'marketsresearch' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'marketsresearch_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function marketsresearch_scripts() {
	wp_enqueue_style( 'marketsresearch-style', get_stylesheet_uri() );

	wp_enqueue_script( 'marketsresearch-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'marketsresearch-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'marketsresearch_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function mr_scripts() {
 
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() .'/css/font-awesome-4.7.0/css/font-awesome.min.css',array(),'4.0.7' );
 
	if( !is_admin()){
	    wp_deregister_script( 'jquery' );
	    wp_register_script('jquery', get_template_directory_uri().'/js/jquery-3.1.1.min.js', false,'3.1.1',true);
	    wp_enqueue_script('jquery');
	}
	
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'), '', true );
	
	wp_enqueue_style( 'mr-style', get_stylesheet_uri() );
 
	wp_enqueue_script( 'mr-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
 
	wp_enqueue_script( 'mr-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
 
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mr_scripts' );

/**
 * navigation bootstrap
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

class CompanyInfoWidget extends WP_Widget
{
  function CompanyInfoWidget()
  {
    $widget_ops = array('classname' => 'CompanyInfoWidget', 'description' => 'Displays the company info' );
    $this->WP_Widget('CompanyInfoWidget', 'Company Information', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE ?>
    <div id="comany_info">
    	<p class="footer-address"><?php echo of_get_option('footer_address'); ?></p>
    	<p class="footer-phone"><a href="tel:<?php echo of_get_option('site_phone'); ?>"><?php echo of_get_option('site_phone'); ?></a></p>
 		<p class="footer-mail"><a href="mailto:<?php echo of_get_option('site_email'); ?>"><?php echo of_get_option('site_email'); ?></a></p>
 	</div>	
   <?php echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("CompanyInfoWidget");') );

class FooterImageWidget extends WP_Widget
{
  function FooterImageWidget()
  {
    $widget_ops = array('classname' => 'FooterImageWidget', 'description' => 'Displays the footer images' );
    $this->WP_Widget('FooterImageWidget', 'Footer Images', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE ?>
    <div id="footer_image">
    	<?php if(of_get_option('footer_image_1')){ ?><p><img src="<?php echo of_get_option('footer_image_1'); ?>" /></p><?php } ?>
    	<?php if(of_get_option('footer_image_2')){ ?><p><img src="<?php echo of_get_option('footer_image_2'); ?>" /></p><?php } ?>
 	</div>	
   <?php echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("FooterImageWidget");') );


/* ==============================================================================================================
					pankaj 
============================================================================================================== */

add_action( 'wp_enqueue_scripts', 'crunchify_enqueue_script' );
function crunchify_enqueue_script() {
    wp_register_script( 'custom_js', get_stylesheet_directory_uri() . '/js/custom_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'custom_js' );

    wp_register_script( 'matchheight_js', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script( 'matchheight_js' );

}


function myprefix_woo_product_sidebar_attributes() {

	// Remove items from summary
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	// Add items to sidebar
	if ( is_singular( 'product' ) ) {
		add_action( 'wpex_hook_sidebar_inner', 'woocommerce_template_single_add_to_cart', 1 );
	}

}
add_action( 'wp', 'myprefix_woo_product_sidebar_attributes' );

function remove_woocommerce_product_tabs( $tabs ) {			
	// unset( $tabs['description'] );
	unset( $tabs['reviews'] );
	// unset( $tabs['additional_information'] );
	return $tabs;		
}
add_filter( 'woocommerce_product_tabs', 'remove_woocommerce_product_tabs', 98 );

add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Report Details' );		// Rename the description tab
	// $tabs['reviews']['title'] = __( 'Ratings' );				// Rename the reviews tab
	$tabs['additional_information']['title'] = __( 'Table of Content' );	// Rename the additional information tab

	return $tabs;

}

add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

	// $tabs['reviews']['priority'] = 5;			// Reviews first
	$tabs['description']['priority'] = 5;			// Description second
	$tabs['additional_information']['priority'] = 10;	// Additional information third

	return $tabs;
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' ); 
add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' );    // < 2.1

function woo_custom_cart_button_text() {
        return __( 'Buy now', 'woocommerce' );
}

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Product Sidebar', 'theme-marketsresearch' ),
        'id' => 'product-sidebar',
        'description' => __( 'Widgets in this area will be shown on all product pages.', 'theme-marketsresearch' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );
}