<?php
 /*
 Template Name: Report Category
 */
 ?>

<?php 
get_header(); ?>
<div class="row">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
			$taxonomy = 'product_cat';
			$args = array(
			    'parent' => 0 // to get only parent terms
			);
			$terms = get_terms( $taxonomy, $args );
		?>

		<div class="panel-group" id="accordion">
		<?php  $counter = 1; 
			foreach( $terms as $term ) { 
				
					$children = get_terms( $taxonomy, array(
					  'parent' => $term->term_id,
					) );
					// echo "<pre>";
					// print_r($children);
					// echo "</pre>";
					$term_link = get_term_link( $term );
				?>
				<div class="col-sm-4 pad20">
		        <div class="panel panel-default custom-category-accordian">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a data-toggle="collapse" data-parent="#accordion" href="<?php echo '#collapse'.$term->term_id; ?>"><?php echo $term->name; ?></a>
		                </h4>
		            </div>
		            <div id="<?php echo 'collapse'.$term->term_id; ?>" class="panel-collapse collapse">
						<div class="panel-body">
							<?php
								echo '<ul class="list-group">';
								foreach ($children as $key => $value) {
									$children_link = get_term_link( $value );
									echo '<li class="list-group-item"><a href="'.esc_url( $children_link ).'">'.$value->name.'</a><span class="badge">'.$value->count.'</span></li>';
								}
								echo '</ul>'; 
							?>	                   
						</div>
		            </div>
		            <div class="term-link">
		            	<?php echo '<div><a href="' . esc_url( $term_link ) . '">See More</a></div>'; ?>
		            </div>
		        </div>
	        </div>
	<?php    
	}
		?>
        
    </div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();